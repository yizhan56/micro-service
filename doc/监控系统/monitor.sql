
USE `monitor`;


DROP TABLE IF EXISTS `cli_info`;

CREATE TABLE `cli_info` (
  `client_id` varchar(32) NOT NULL COMMENT '客户端编号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `remark` varchar(200) default NULL COMMENT '描叙',
  `ip` varchar(30) NOT NULL COMMENT 'ip地址',
  `port` int(11) NOT NULL COMMENT '端口',
  `token` varchar(100) NOT NULL COMMENT '密钥',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `user_id` int(11) NOT NULL COMMENT '添加人',
  `status` int(11) NOT NULL COMMENT '状态[10正常、20停用]',
  `activity_status` int(11) default NULL COMMENT '活动状态[10正常、20心跳异常]',
  `activity_time` datetime default NULL COMMENT '上次活动时间',
  PRIMARY KEY  (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端表';


DROP TABLE IF EXISTS `code_create`;

CREATE TABLE `code_create` (
  `id` int(11) NOT NULL auto_increment COMMENT '编号',
  `code` varchar(50) NOT NULL COMMENT '源码编号',
  `package_path` varchar(200) NOT NULL COMMENT '功能包路径',
  `status` int(11) NOT NULL COMMENT '状态[10待生成、20生成中、30生成失败、40生成成功]',
  `download` varchar(200) default NULL COMMENT '下载地址',
  `finish_time` datetime default NULL COMMENT '生成完成时间',
  `ds_code` varchar(100) NOT NULL COMMENT '数据源编号',
  `db_name` varchar(100) NOT NULL COMMENT '数据库名',
  `tables` varchar(200) NOT NULL COMMENT '生成的表集合[多个,分隔]',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `user_id` int(11) NOT NULL COMMENT '创建人',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生成源码表';


DROP TABLE IF EXISTS `code_prj`;

CREATE TABLE `code_prj` (
  `code` varchar(50) NOT NULL COMMENT '编码',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `name` varchar(150) NOT NULL COMMENT '名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `user_id` int(11) NOT NULL COMMENT '创建人',
  PRIMARY KEY  (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目源码表';


DROP TABLE IF EXISTS `code_template`;

CREATE TABLE `code_template` (
  `code` varchar(50) NOT NULL COMMENT '源码编号',
  `type` int(11) NOT NULL COMMENT '类型[10java、20jsp、30其它文件]',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `remark` varchar(200) default NULL COMMENT '描叙',
  `package_name` varchar(30) NOT NULL COMMENT '包名',
  `content` text NOT NULL COMMENT '模板内容',
  `path` varchar(200) NOT NULL COMMENT '模板路劲',
  `suffix` varchar(50) default NULL COMMENT '文件后缀',
  PRIMARY KEY  (`code`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目模板表';


DROP TABLE IF EXISTS `ms_config`;

CREATE TABLE `ms_config` (
  `config_id` int(11) NOT NULL auto_increment COMMENT '编号',
  `name` varchar(100) NOT NULL COMMENT '文件名称',
  `remark` varchar(250) default NULL COMMENT '备注',
  `is_use` int(11) NOT NULL COMMENT '是否使用',
  `create_time` datetime NOT NULL COMMENT '创建日期',
  `user_id` int(11) NOT NULL COMMENT '添加人',
  `prj_id` int(11) NOT NULL default '0' COMMENT '项目编号',
  PRIMARY KEY  (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置文件表';


DROP TABLE IF EXISTS `ms_config_value`;

CREATE TABLE `ms_config_value` (
  `config_id` int(11) NOT NULL COMMENT '配置编号',
  `code` varchar(150) NOT NULL COMMENT 'key的编码',
  `value` varchar(250) default NULL COMMENT 'value',
  `remark` varchar(250) default NULL COMMENT '备注',
  `orderby` int(11) NOT NULL default '0' COMMENT '排序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `user_id` int(11) NOT NULL COMMENT '添加人',
  PRIMARY KEY  (`config_id`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置文件值表';


DROP TABLE IF EXISTS `ms_secret`;

CREATE TABLE `ms_secret` (
  `cli_id` varchar(32) NOT NULL default '' COMMENT '客户端编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `remark` varchar(100) default NULL COMMENT '备注',
  `token` varchar(100) NOT NULL COMMENT '密钥',
  `domain` varchar(200) default NULL COMMENT '主路径',
  `is_use` int(11) NOT NULL COMMENT '是否使用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `req_max_hour` bigint(20) NOT NULL default '0' COMMENT '每小时最大请求[0代表不限制]',
  `req_max_second` bigint(20) NOT NULL default '0' COMMENT '每秒最大请求数[0代表不限制]',
  PRIMARY KEY  (`cli_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应用密钥表';


DROP TABLE IF EXISTS `ms_secret_api`;

CREATE TABLE `ms_secret_api` (
  `cli_id` varchar(32) NOT NULL COMMENT '客户端编号',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `prj_code` varchar(50) NOT NULL COMMENT '项目编码',
  `url` varchar(150) NOT NULL COMMENT 'API地址',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY  (`cli_id`,`prj_id`,`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='密钥API权限表';


DROP TABLE IF EXISTS `prj_ant`;

CREATE TABLE `prj_ant` (
  `pa_id` int(11) NOT NULL auto_increment COMMENT '编号',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `pid` int(11) NOT NULL COMMENT '父编号',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `img` varchar(150) default NULL COMMENT '图片',
  `api_text` text COMMENT 'api内容',
  PRIMARY KEY  (`pa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目原型表';


DROP TABLE IF EXISTS `prj_api`;

CREATE TABLE `prj_api` (
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `path` varchar(255) NOT NULL COMMENT '路径',
  `name` varchar(200) default NULL COMMENT '名称',
  `method` varchar(500) NOT NULL COMMENT '方法详情',
  `params` text COMMENT '参数',
  `response` text COMMENT '结果',
  `is_use` int(11) NOT NULL COMMENT '是否使用',
  `create_time` datetime NOT NULL COMMENT '新增时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY  (`prj_id`,`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目的API表';


DROP TABLE IF EXISTS `prj_api_test`;

CREATE TABLE `prj_api_test` (
  `id` int(11) NOT NULL auto_increment COMMENT '编号',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `name` varchar(100) NOT NULL COMMENT '测试名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `test_time` datetime default NULL COMMENT '测试时间',
  `test_result` varchar(255) default NULL COMMENT '测试结果',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目API测试表';


DROP TABLE IF EXISTS `prj_api_test_dtl`;

CREATE TABLE `prj_api_test_dtl` (
  `pat_id` int(11) NOT NULL COMMENT '项目API测试编号',
  `path` varchar(150) NOT NULL COMMENT '项目API路径',
  `params` text NOT NULL COMMENT '参数',
  `status` int(11) NOT NULL COMMENT '状态[10待测试、20测试中、30成功、40失败]',
  `succ_cond` varchar(255) NOT NULL COMMENT '成功条件',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `test_time` datetime default NULL COMMENT '测试时间',
  `test_result` varchar(255) default NULL COMMENT '测试结果',
  PRIMARY KEY  (`pat_id`,`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目API测试接口表';


DROP TABLE IF EXISTS `prj_client`;

CREATE TABLE `prj_client` (
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `client_id` varchar(32) NOT NULL COMMENT '客户端编号',
  `version` varchar(50) NOT NULL COMMENT '版本编号',
  `status` int(11) NOT NULL COMMENT '状态[10待发布、20发布中、30发布失败、40发布成功]',
  `status_msg` varchar(200) default NULL COMMENT '状态消息',
  `release_time` datetime default NULL COMMENT '发布时间',
  `shell_script` text COMMENT '客户端执行的Shell命令',
  `log_path` varchar(150) default NULL COMMENT '日志路径',
  PRIMARY KEY  (`prj_id`,`client_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目客户端表';


DROP TABLE IF EXISTS `prj_ds`;

CREATE TABLE `prj_ds` (
  `code` varchar(32) NOT NULL COMMENT '数据源编码',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `type` varchar(30) NOT NULL COMMENT '数据库类型[mysql/oracle]',
  `driver_class` varchar(100) NOT NULL COMMENT '驱动类',
  `url` varchar(100) NOT NULL COMMENT 'jdbc的url',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `initial_size` int(11) NOT NULL COMMENT '初始连接数',
  `max_idle` int(11) NOT NULL COMMENT '最大连接数',
  `min_idle` int(11) NOT NULL COMMENT '最小连接数',
  `test_sql` varchar(200) NOT NULL COMMENT '测试的sql语句',
  `user_id` int(11) NOT NULL COMMENT '添加人',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY  (`code`,`prj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目数据源表';


DROP TABLE IF EXISTS `prj_info`;

CREATE TABLE `prj_info` (
  `prj_id` int(11) NOT NULL auto_increment COMMENT '编号',
  `code` varchar(50) NOT NULL COMMENT '项目编码',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `remark` varchar(200) default NULL COMMENT '描叙',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `user_id` int(11) NOT NULL COMMENT '添加人',
  `release_version` varchar(50) default NULL COMMENT '发布的版本号',
  `release_time` datetime default NULL COMMENT '发布的版本时间',
  `status` int(11) NOT NULL COMMENT '状态[10正常、20停用]',
  `container` int(11) NOT NULL COMMENT '容器类型[10tomcat、50自定义服务、100其它]',
  `shell_script` text COMMENT 'shell脚本',
  `monitor_status` int(11) NOT NULL default '0' COMMENT '监控状态是否正常',
  `monitor_msg` varchar(200) default NULL COMMENT '监控消息',
  PRIMARY KEY  (`prj_id`),
  UNIQUE KEY `UQ_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目表';


DROP TABLE IF EXISTS `prj_monitor`;

CREATE TABLE `prj_monitor` (
  `prjm_id` int(11) NOT NULL auto_increment COMMENT '编号',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `type` int(11) NOT NULL COMMENT '监控类型[10服务、20数据库、30缓存、40其它]',
  `remark` varchar(100) NOT NULL COMMENT '描叙',
  `monitor_is` int(11) NOT NULL COMMENT '是否检测',
  `monitor_succ_str` varchar(100) default NULL COMMENT '检测成功标识',
  `monitor_status` int(11) default NULL COMMENT '检测状态[10正常、20异常]',
  `monitor_url` varchar(150) default NULL COMMENT '检测地址',
  `monitor_time` datetime default NULL COMMENT '检测时间',
  `monitor_fail_num` int(11) default NULL COMMENT '检测失败次数',
  `monitor_fail_num_remind` int(11) default NULL COMMENT '检测失败最大次数提醒[0代表不提醒]',
  `monitor_fail_email` varchar(50) default NULL COMMENT '检测失败接收邮箱',
  `monitor_fail_send_time` datetime default NULL COMMENT '检测失败发送信息时间',
  `monitor_fail_send_interval` int(11) default NULL COMMENT '检测失败发送信息间隔[单位：分钟]',
  PRIMARY KEY  (`prjm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目监控表';


DROP TABLE IF EXISTS `prj_optimize`;

CREATE TABLE `prj_optimize` (
  `id` int(11) NOT NULL auto_increment COMMENT '编码',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `url` varchar(200) NOT NULL COMMENT '接口地址',
  `warn_time` int(11) NOT NULL COMMENT '预警时间[单位ms]',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目优化规则表';


DROP TABLE IF EXISTS `prj_optimize_log`;

CREATE TABLE `prj_optimize_log` (
  `id` int(11) NOT NULL auto_increment COMMENT '编号',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `url` varchar(200) NOT NULL COMMENT '接口地址',
  `params` text COMMENT '请求参数',
  `req_time` datetime NOT NULL COMMENT '请求时间',
  `res_time` datetime NOT NULL COMMENT '响应时间',
  `use_time` int(11) default NULL COMMENT '用时[单位ms]',
  `res_result` longtext COMMENT '响应结果',
  `res_size` float default NULL COMMENT '响应数据大小[单位kb]',
  `remark` text COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目优化记录表';


DROP TABLE IF EXISTS `prj_version`;

CREATE TABLE `prj_version` (
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `version` varchar(50) NOT NULL COMMENT '版本号',
  `remark` varchar(300) default NULL COMMENT '描叙',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `user_id` int(11) NOT NULL COMMENT '添加人',
  `is_release` int(11) NOT NULL COMMENT '是否发布',
  `path_url` varchar(200) NOT NULL COMMENT '版本所在的路径',
  `rb_version` varchar(50) default NULL COMMENT '回滚版本',
  `is_rel_time` int(11) NOT NULL COMMENT '是否定时发布',
  `rel_time` datetime NOT NULL COMMENT '定时发布时间',
  `rel_status` int(11) NOT NULL COMMENT '定时发布状态[10待发布、20发布中，30发布失败、40发布成功]',
  `rel_msg` varchar(250) default NULL COMMENT '定时发布结果',
  PRIMARY KEY  (`prj_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目版本表';


DROP TABLE IF EXISTS `prj_version_script`;

CREATE TABLE `prj_version_script` (
  `pvs_id` int(11) NOT NULL auto_increment COMMENT '编号',
  `prj_id` int(11) NOT NULL COMMENT '项目编号',
  `version` varchar(32) NOT NULL COMMENT '版本号',
  `remark` varchar(100) default NULL COMMENT '备注',
  `ds_code` varchar(32) NOT NULL COMMENT '数据源编号',
  `up_sql` text NOT NULL COMMENT '升级脚本',
  `callback_sql` text COMMENT '回滚脚本',
  `is_up` int(11) NOT NULL COMMENT '是否升级',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `user_id` int(11) NOT NULL COMMENT '添加人',
  PRIMARY KEY  (`pvs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目版本脚本表';


DROP TABLE IF EXISTS `sys_config`;

CREATE TABLE `sys_config` (
  `code` varchar(50) NOT NULL COMMENT '编码',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `value` varchar(100) NOT NULL COMMENT '值',
  `remark` varchar(100) default NULL COMMENT '描叙',
  `exp1` varchar(100) default NULL COMMENT '扩展1',
  `exp2` varchar(100) default NULL COMMENT '扩展2',
  PRIMARY KEY  (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sys_file`;

CREATE TABLE `sys_file` (
  `file_id` varchar(32) NOT NULL COMMENT '编号',
  `type` int(11) NOT NULL COMMENT '类型[10项目]',
  `org_name` varchar(80) NOT NULL COMMENT '原名称',
  `sys_name` varchar(80) NOT NULL COMMENT '系统名称',
  `url` varchar(200) NOT NULL COMMENT '显示路径',
  `file_type` varchar(20) NOT NULL COMMENT '文件类型',
  `file_size` float NOT NULL COMMENT '文件大小',
  `status` int(11) NOT NULL COMMENT '状态[0待确定、1使用中、2未使用、3已作废]',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY  (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统文件表';


DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL auto_increment COMMENT '编号',
  `username` varchar(30) NOT NULL COMMENT '用户名',
  `password` varchar(80) NOT NULL COMMENT '密码',
  `nickname` varchar(30) NOT NULL COMMENT '昵称',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `status` int(11) NOT NULL COMMENT '状态[10正常、20冻结]',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `unique_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sys_user_att`;

CREATE TABLE `sys_user_att` (
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  `type` varchar(20) NOT NULL COMMENT '类型[10项目]',
  `type_no` varchar(32) NOT NULL COMMENT '类型编码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY  (`user_id`,`type`,`type_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='我的关注表';

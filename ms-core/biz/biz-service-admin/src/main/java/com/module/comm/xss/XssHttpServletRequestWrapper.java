package com.module.comm.xss;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.springframework.web.util.HtmlUtils;

import com.system.comm.utils.FrameStringUtil;

public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {

	public XssHttpServletRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public String getHeader(String name) {
		if (FrameStringUtil.isEmpty(name)) {
			return super.getHeader(name);
		}
		String value = super.getHeader(name);
		if (FrameStringUtil.isEmpty(value)) {
			return value;
		}
		return HtmlUtils.htmlEscape(value);
		//return StringEscapeUtils.escapeHtml4(super.getHeader(name));
	}

	@Override
	public String getQueryString() {
		String value = super.getQueryString();
		if (FrameStringUtil.isEmpty(value)) {
			return value;
		}
		return HtmlUtils.htmlEscape(value);
		//return StringEscapeUtils.escapeHtml4(super.getQueryString());
	}

	@Override
	public String getParameter(String name) {
		String value = super.getParameter(name);
		if (FrameStringUtil.isEmpty(value)) {
			return value;
		}
		return HtmlUtils.htmlEscape(value);
		//return StringEscapeUtils.escapeHtml4(super.getParameter(name));
	}

	@Override
	public String[] getParameterValues(String name) {
		String[] values = super.getParameterValues(name);
		if(values != null) {
			int length = values.length;
			String[] escapseValues = new String[length];
			for(int i = 0; i < length; i++){
				String value = values[i];
				if (FrameStringUtil.isNotEmpty(value)) {
					//escapseValues[i] = StringEscapeUtils.escapeHtml4(values[i]);
					escapseValues[i] = HtmlUtils.htmlEscape(value);
				}
			}
			return escapseValues;
		}
		return super.getParameterValues(name);
	}

}
package com.module.admin.prj.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.prj.pojo.PrjApiTest;
import com.module.admin.prj.pojo.PrjApiTestDtl;
import com.module.admin.prj.service.PrjApiService;
import com.module.admin.prj.service.PrjApiTestDtlService;
import com.module.admin.prj.service.PrjApiTestService;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * prj_api_dtl测试的Controller
 * @author yuejing
 * @date 2016-11-30 13:30:00
 * @version V1.0.0
 */
@Controller
public class PrjApiTestDtlController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrjApiTestDtlController.class);

	@Autowired
	private PrjApiTestDtlService prjApiTestDtlService;
	@Autowired
	private PrjApiTestService prjApiTestService;
	@Autowired
	private PrjApiService prjApiService;

	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	/*@RequestMapping(value = "/prjApiTest/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		return "admin/prj/apiTest-manager";
	}*/

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/prjApiTestDtl/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			PrjApiTestDtl prjApiTestDtl) {
		ResponseFrame frame = null;
		try {
			frame = prjApiTestDtlService.pageQuery(prjApiTestDtl);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/prjApiTestDtl/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap,
			Integer patId, String path) {
		PrjApiTest prjApiTest = prjApiTestService.get(patId);
		modelMap.put("prjApiTest", prjApiTest);
		if(patId != null && FrameStringUtil.isNotEmpty(path)) {
			modelMap.put("prjApiTestDtl", prjApiTestDtlService.get(patId, path));
			modelMap.put("apiString", FrameJsonUtil.toString(prjApiService.get(prjApiTest.getPrjId(), path)));
		}
		/*List<PrjApi> prjApis = prjApiService.findByPrjId(prjApiTest.getPrjId());
		modelMap.put("prjApiString", FrameJsonUtil.toString(prjApis));*/
		return "admin/prj/apiTestDtl-edit";
	}
	
	@RequestMapping(value = "/prjApiTestDtl/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response,
			PrjApiTestDtl prjApiTestDtl) {
		ResponseFrame frame = null;
		try {
			frame = prjApiTestDtlService.saveOrUpdate(prjApiTestDtl);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/prjApiTestDtl/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response,
			Integer patId, String path) {
		ResponseFrame frame = null;
		try {
			frame = prjApiTestDtlService.delete(patId, path);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
}
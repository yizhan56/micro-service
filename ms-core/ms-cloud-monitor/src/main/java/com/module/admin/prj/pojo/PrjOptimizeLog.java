package com.module.admin.prj.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * prj_optimize_log实体
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
@Alias("prjOptimizeLog")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class PrjOptimizeLog extends BaseEntity implements Serializable {
	//编号
	private Integer id;
	//项目编号
	private Integer prjId;
	//接口地址
	private String url;
	//请求参数
	private String params;
	//请求时间
	private Date reqTime;
	//响应时间
	private Date resTime;
	//用时[单位ms]
	private Integer useTime;
	//响应结果
	private String resResult;
	//响应数据大小[单位kb]
	private Double resSize;
	//备注
	private String remark;
	//创建时间
	private Date createTime;
	
	//============================ 扩展属性
	//项目名称
	private String prjName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getPrjId() {
		return prjId;
	}
	public void setPrjId(Integer prjId) {
		this.prjId = prjId;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	
	public Date getReqTime() {
		return reqTime;
	}
	public void setReqTime(Date reqTime) {
		this.reqTime = reqTime;
	}
	
	public Date getResTime() {
		return resTime;
	}
	public void setResTime(Date resTime) {
		this.resTime = resTime;
	}
	
	public Integer getUseTime() {
		return useTime;
	}
	public void setUseTime(Integer useTime) {
		this.useTime = useTime;
	}
	
	public String getResResult() {
		return resResult;
	}
	public void setResResult(String resResult) {
		this.resResult = resResult;
	}
	
	public Double getResSize() {
		return resSize;
	}
	public void setResSize(Double resSize) {
		this.resSize = resSize;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getPrjName() {
		return prjName;
	}
	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}
}
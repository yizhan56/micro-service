package com.system.comm.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 操作request的信息
 * @author yuejing
 *
 */
public class FrameHttpRequestUtil {

    @SuppressWarnings("rawtypes")
    private static final ThreadLocal REQ_BODY_PARAMS_LOCAL = new ThreadLocal();
    
	/**
	 * 获取request的body请求的内容，返回Map
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getReqBodyParams(HttpServletRequest request) {
		try {
			String reqBodyStr = "";
			String reqBodyParams = (String) REQ_BODY_PARAMS_LOCAL.get();
			if(FrameStringUtil.isEmpty(reqBodyParams)) {
				BufferedReader br = request.getReader();
				String str = "";
				while((str = br.readLine()) != null) {
					reqBodyStr += str;
				}
				REQ_BODY_PARAMS_LOCAL.set(reqBodyStr);
			} else {
				reqBodyStr = (String) REQ_BODY_PARAMS_LOCAL.get();
			}
			Map<String, Object> map = FrameJsonUtil.toMap(reqBodyStr);
			return map;
		} catch (IOException e) {
			REQ_BODY_PARAMS_LOCAL.remove();
			return new HashMap<String, Object>(0);
		}
	}
}
